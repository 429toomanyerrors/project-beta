import React from 'react';

class VehicleModelDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      models: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleVehicleModelSelection = this.handleVehicleModelSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const locationUrl = `http://localhost:8100/api/models/${this.state.model}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newVehicleModel = await response.json();
      console.log(newVehicleModel)

      
      let remainingVehicleModels = []
      for (let i = 0; i < this.state.models.length; i++) {
        let currentVehicleModels = this.state.models[i]
        if (parseInt(this.state.model) !== currentVehicleModels.id) {
          remainingVehicleModels.push(currentVehicleModels)
        }
      }

      this.setState({
        model: '',
        models: remainingVehicleModels
      })
    }
  }

  handleVehicleModelSelection(event) {
    const value = event.target.value;
    this.setState({ model: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Vehicle Model To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="mb-3">
                <select onChange={this.handleVehicleModelSelection} value={this.state.model} required name="model" id="model" className="form-select">
                  <option value="">Choose Vehicle Model</option>
                  {this.state.models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleModelDelete;