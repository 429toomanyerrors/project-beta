import React from 'react';

class ManufacturerDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturers: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleManufacturerSelection = this.handleManufacturerSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const locationUrl = `http://localhost:8100/api/manufacturers/${this.state.manufacturer}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer)

      
      let reaminingManufacturers = []
      for (let i = 0; i < this.state.manufacturers.length; i++) {
        let currentManufacturer = this.state.manufacturers[i]
        if (parseInt(this.state.manufacturer) !== currentManufacturer.id) {
          reaminingManufacturers.push(currentManufacturer)
        }
      }

      this.setState({
        manufacturer: '',
        manufacturers: reaminingManufacturers
      })
    }
  }

  handleManufacturerSelection(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Manufacturers To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
              <div className="mb-3">
                <select onChange={this.handleManufacturerSelection} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                  <option value="">Choose Manufacturer</option>
                  {this.state.manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ManufacturerDelete;
