import React from 'react';

class AutomobileDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobiles: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAutomobileSelection = this.handleAutomobileSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ automobiles: data.automobiles });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const automobileURL = `http://localhost:8100/api/automobiles/${this.state.automobile}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(automobileURL, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile)

      
      let remainingAutomobiles = []
      for (let i = 0; i < this.state.automobiles.length; i++) {
        let currentAutomobiles = this.state.automobiles[i]
        if (parseInt(this.state.automobile) !== currentAutomobiles.id) {
          remainingAutomobiles.push(currentAutomobiles)
        }
      }

      this.setState({
        automobile: '',
        automobiles: remainingAutomobiles
      })
    }
  }

  handleAutomobileSelection(event) {
    const value = event.target.value;
    this.setState({ automobile: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Which customer's cars would you like to remove?</h1>
            <form onSubmit={this.handleSubmit} id="delete-automobile-form">
              <div className="mb-3">
                <select onChange={this.handleAutomobileSelection} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an automobile</option>
                  {this.state.automobiles.map(automobile => {
                    return (
                      <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileDelete;
