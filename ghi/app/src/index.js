import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadInventory() {
  let manufacturerData, vehicleModelData, customerData, automobileData, technicianData, ServiceAppointmentData, salespersondata, salesdata;
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const vehicleModelResponse = await fetch('http://localhost:8100/api/models/');
  const automobileResponse = await fetch ('http://localhost:8100/api/automobiles/');
  const technicianResponse = await fetch(' http://localhost:8080/api/technicians/');
  const serviceAppointmentResponse = await fetch('http://localhost:8080/api/services/')
 
  


  if (customerResponse.ok) {
    customerData = await customerResponse.json();
  } else {
    console.error(customerResponse);
  }
  if (salesdataResponse.ok) {
    salesdata = await salesdataResponse.json();
    console.log('sales data: ', salesdata)
  } else {
    console.error(salesdataResponse);
  }
  if (salespersonResponse.ok) {
    salespersondata = await salespersonResponse.json();
    console.log('salesperson data: ', salespersondata)
  } else {
    console.error(salespersonResponse);
  }
  if (manufacturerResponse.ok) {
    manufacturerData = await manufacturerResponse.json();
    console.log('manufacturer data: ', manufacturerData)
  } else {
    console.error(manufacturerResponse);
  }
  if (vehicleModelResponse.ok) {
    vehicleModelData = await vehicleModelResponse.json();
    console.log('vehicle model data: ', vehicleModelData)
  } else {
    console.error(vehicleModelResponse);
  }
  if (automobileResponse.ok) {
    automobileData = await automobileResponse.json();
    console.log('automobile data: ', automobileData)
  } else {
    console.error(automobileResponse);
  }
  if (technicianResponse.ok) {
    technicianData = await technicianResponse.json();
    console.log('technician data', technicianData)
  } else {
    console.error(technicianResponse)
  }
  if (serviceAppointmentResponse.ok) {
    ServiceAppointmentData = await serviceAppointmentResponse.json();
    console.log('service appointment data ', ServiceAppointmentData)
  } else {
    console.error(serviceAppointmentResponse)
  }

  root.render(
    <React.StrictMode>
      <App automobiles={automobileData.automobiles} models = {vehicleModelData.models} 
      manufacturers= {manufacturerData.manufacturers}
      service_appointments={ServiceAppointmentData.service_appointments}
      salespersons= {salespersondata} sales={salesdata.sales}
       customers={customerData}
      />
    </React.StrictMode>
  );
}
loadInventory();