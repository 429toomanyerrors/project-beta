import React from "react";

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_number: "",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        
        const technicianURL = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'appication/json',
          },
        };
        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
          const newTechnician = await response.json();
          console.log(newTechnician)
          this.setState({
            name: "",
            employee_number: ""
          });
        }
      }           
    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ name: value})
    }
    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({ employee_number: value})
    }
    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new Technician</h1>
                <form onSubmit={this.handleSubmit} id="create-technician-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeTechnician} value={this.state.name} placeholder="Technician" required type="text" name="technician" id="technician" className="form-control" />
                    <label htmlFor="technician">Technician</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeEmployeeNumber} value={this.state.employee_number} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                    <label htmlFor="technician">Employee Number</label>
                  </div>
                  <button className="btn btn-primary">Add</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}

export default TechnicianForm;