function ServiceAppointmentList(props) {
    console.log("SAList",props)
    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th className="display-5 fw-bold">Service Appointments</th>
            </tr>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody >
            {props.service_appointments.map(appointment => {
                return (
                    <tr key={ appointment.id}>
                        <td> {appointment.vinVO} </td>
                        <td> {appointment.owners_name} </td>
                        <td> {appointment.Date} </td>
                        <td> {appointment.Time} </td>
                        <td> {appointment.technicians.name} </td>
                        <td> {appointment.reason} </td>
                    </tr>
                );
            })}
        </tbody>
      </table> 
    );
  }

  export default ServiceAppointmentList;