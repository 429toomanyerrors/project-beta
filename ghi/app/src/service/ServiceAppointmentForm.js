import React from 'react';
class ServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            owners_name: "",
            vin: "",
            date: "",
            time: "",
            reason: "",
            technicians: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeOwnersName = this.handleChangeOwnersName.bind(this);
        this.handleChangeVIN = this.handleChangeVIN.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeReason = this.handleChangeReason.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
    } 

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log("tech data", data)
            this.setState({ technicians: data.technicians })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log("333", data)
        delete data.services
        delete data.technicians

        const serviceAppointmentURL = 'http://localhost:8080/api/services/'
        console.log("fetch", serviceAppointmentURL )
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type' : 'application/json',
            },
        };
        const response = await fetch(serviceAppointmentURL, fetchConfig);
        console.log("last", response)
        if (response.ok) {
            const newServiceAppointment = await response.json();
            console.log(newServiceAppointment)

            this.setState({
                owners_name: "",
                vin: "",
                date: "",
                time:"",
                reason: "",
                technician: [] 
            });
        }
    }

    handleChangeOwnersName(event) {
        const value = event.target.value;
        this.setState({ owners_name: value });
    }

    handleChangeVIN(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleChangeDate(event) {
        const value = event.target.value;
        this.setState({ date: value });
    }

    handleChangeTime(event) {
      const value = event.target.value;
      this.setState({ time: value });
  }

    handleChangeReason(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }

    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician_id: value });
    }

    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Make an appointment</h1>
                <form onSubmit={this.handleSubmit} id="create-service-appointment-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeOwnersName} value={this.state.name} placeholder="Owners Name" required type="text" name="owners_name" id="owners_name" className="form-control" />
                    <label htmlFor="name">Owner's Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeDate} value={this.state.date} placeholder="date" required type="date" name="date" id="date" className="form-select" />
                    <label htmlFor="date">Choose a Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeTime} value={this.state.time} placeholder="Time" required type="time"   name="time" id="time" className="form-select" />
                    <label htmlFor="time">Prefered Time</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeVIN} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                    <label htmlFor="reason">Reason</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeTechnician} value={this.state.technician} required name="technician" id="technician" className="form-select">
                        <option value="">Choose Technician</option>
                        {this.state.technicians.map(technician => {
                            return (
                                <option key={technician.id} value={technician.id}>{technician.name}</option>
                            )
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }

}
export default ServiceAppointmentForm;