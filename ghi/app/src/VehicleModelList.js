function VehicleModelList(props) {
    console.log(props)
    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th className="display-5 fw-bold">Vehicle Models</th>
            </tr>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody >
            {props.models.map(model => {
                return (
                    <tr key={model.id}>
                        <td> {model.name} </td>
                        <td> {model.manufacturer.name} </td>
                        <td> <img src={model.picture_url} className="img-fluid" alt="..."/> </td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    );
  }
  
export default VehicleModelList;