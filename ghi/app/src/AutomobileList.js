function AutomobileList(props) {
  console.log(props)
  return (
    <table className="table table-striped">
      <thead>
          <tr>
              <th className="display-5 fw-bold">Vehicle Models</th>
          </tr>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody >
          {props.automobiles.map(automobile => {
              return (
                  <tr key={automobile.id}>
                      <td> {automobile.vin} </td>
                      <td> {automobile.color} </td>
                      <td> {automobile.year} </td>
                      <td> {automobile.model.name} </td>
                      <td> {automobile.model.manufacturer.name} </td>
                  </tr>
              );
          })}
      </tbody>
    </table>
  );
}

export default AutomobileList;