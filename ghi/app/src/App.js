import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerDelete from './ManufacturerDelete';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelDelete from './VehicleModelDelete';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import AutomobileDelete from './AutomobileDelete';

import ServiceAppointmentList from './service/ServiceAppointmetList';
import TechnicianForm from './service/TechnicianForm';
import ServiceAppointmentForm from './service/ServiceAppointmentForm';



import CustomerForm from './Sales/CustomerForm';
import SalespersonForm from './Sales/SalespersonForm';
import SalesForm from './Sales/SalesForm';
import SalesList from './Sales/SalesList';
import SalesHistoryList from './Sales/SalesHistoryList';
import CustomerList from './Sales/CustomerList';
import SalesPersonList from './Sales/SalesPersonList';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        <Route path="manufacturers">
          <Route index element={<ManufacturerList manufacturers={props.manufacturers} />} />
          <Route path="new" element={<ManufacturerForm />} />
          <Route path="delete" element={<ManufacturerDelete />} />
        </Route>
        <Route path="models">
          <Route index element={<VehicleModelList models={props.models} />} />
          <Route path="new" element={<VehicleModelForm />} />
          <Route path="delete" element={<VehicleModelDelete />} />
        </Route>
        <Route path="automobiles">
          <Route index element={<AutomobileList automobiles={props.automobiles} />} />
          <Route path="new" element={<AutomobileForm />} />
          <Route path="delete" element={<AutomobileDelete />} />
        </Route>
        <Route path="technicians">
          <Route index element={<TechnicianForm technicians={props.technicians} />} />
        </Route>
        <Route path="services">
          <Route index element={<ServiceAppointmentForm service_appointments={props.service_appointments} />} />
          <Route path="appointments" element= {<ServiceAppointmentList service_appointments={props.service_appointments} />} />
        </Route>
        <Route path="customers">
          <Route index element={<CustomerList customers={props.customers} />} />
          <Route path="new" element={<CustomerForm />} />
        </Route>
        <Route path="salespersons">
          <Route index element={<SalesPersonList salespersons={props.salespersons} />} />
          <Route path="new" element={<SalespersonForm />} />
        </Route>
        <Route path="sales">
          <Route index element={<SalesList sales={props.sales} />} />
          <Route path="new" element={<SalesForm />} />
        </Route>
        <Route path="salehistory">
          <Route index element={<SalesHistoryList salehistory={props.salehistory} />} />
        </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
