import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/delete">Remove Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Add Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/delete">Remove Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Add Automobile </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/delete">Remove Automobile</NavLink>
            </li>
            
        <div className="p-2 dropdown">
          <NavLink
            className="btn btn-secondary dropdown-toggle"
            to="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sales
          </NavLink>

            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <NavLink className="dropdown-item" to="/salespersons/new">
              New Salesperson
            </NavLink>
            <NavLink className="dropdown-item" to="/salespersons/">
              Employee List
            </NavLink>
            <NavLink className="dropdown-item" to="/customers/new">
              New Customer
            </NavLink>
            <NavLink className="dropdown-item" to="/customers/">
              Customer Lists
            </NavLink>
            <NavLink className="dropdown-item" to="/sales/">
              Sales List
            </NavLink>
            <NavLink className="dropdown-item" to="/sales/history">
              Salesperson History
            </NavLink>
            </div>
          </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
