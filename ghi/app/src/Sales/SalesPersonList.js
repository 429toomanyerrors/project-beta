function SalesPersonList(props) {
    console.log(props)
    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th className="display-5 fw-bold"> Sales Persons</th>
            </tr>
          <tr>
            <th>Name</th>
            <th>Salesperson ID</th>
          </tr>
        </thead>
        <tbody >
            {props.salespersons.map(salesperson => {
                return (
                    <tr key={salesperson.id}>
                        <td> {salesperson.name} </td>
                        <td> {salesperson.salespersonid} </td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    );
  }
  
export default SalesPersonList;