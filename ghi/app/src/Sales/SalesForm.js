import React from "react";

class SalesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: '',
      salespersons: [],
      automobiles: [],
      customers: [],
    };

    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeSalespersons= this.handleChangeSalespersons.bind(this);
    this.handleChangeAutomobiles= this.handleChangeAutomobiles.bind(this);
    this.handleChangeCustomers= this.handleChangeCustomers.bind(this);
    this.handleChangePrice= this.handleChangePrice.bind(this);
  }

  async componentDidMount() {
    const salespersonUrl = "http://localhost:8090/api/salespersons/";
    const customerUrl = "http://localhost:8090/api/customers/";
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const soldUrl = "http://localhost:8090/api/sales/";

    
    const salespersonResponse = await fetch(salespersonUrl);
    const customerResponse = await fetch(customerUrl);
    const automobileResponse = await fetch(automobileUrl);
    const soldResponse = await fetch(soldUrl);
    if (
        salespersonResponse.ok &&
        customerResponse.ok &&
        automobileResponse.ok &&
        soldResponse.ok
        // && made this so easy, shoutout to the seers
      ) {
        const salespersondata = await salespersonResponse.json();
        const customerdata = await customerResponse.json();
        const automobiledata = await automobileResponse.json();
        const solddata = await soldResponse.json();
  
        const unsoldcars = automobiledata.automobiles;
        const soldcars = solddata.sales;
        // making a list of unsold cars to populate our dropdown

        const beensold = soldcars.reduce((i, soldcar) => {
            i[soldcar.automobile] = true;
            return i
        },{});
        // stolen from grepper and combined with mdn web docs which just has arrow syntax laid out
        const availablecars = unsoldcars.filter((i) => !beensold[i.vin]);
        // if the car hasn't been sold, list it
        this.setState({
            salespersons: salespersondata,
            customers: customerdata,
            automobiles: availablecars,
        });
    }
  }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.automobiles;
        delete data.salespersons;
        delete data.customers;
        console.log('this is a string', this.state)

        const soldUrl= 'http://localhost:8090/api/sales/'
        const fetchConfig= {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(soldUrl, fetchConfig);
        // i wonder if i can just pass in soldurl from my previous decleration
        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);
            this.setState({
              salesperson: [],
              automobile: [],
              customer: [],
              price: '',
            });
        }
    }

    handleChangeSalespersons(event) {
        const value = event.target.value;
        this.setState({ salesperson: value });
    }
    handleChangeAutomobiles(event) {
        const value = event.target.value;
        this.setState({ automobile: value });
    }
    handleChangeCustomers(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }
    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value });
    }
    render() {
      console.log('*****',this.state.customers)
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Transaction.</h1>
                <form onSubmit={this.handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangePrice} value={this.state.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeCustomers}  required id="customer" name="customer" className="form-select">
                        <option value="">Choose a customer</option>
                        {this.state.customers.map(customer => {
                            return (
                            <option key={customer.id} value={customer.name}>
                                {customer.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeAutomobiles}  required id="automobile" name="automobile" className="form-select">
                        <option value="">Choose an Automobile</option>
                        {this.state.automobiles.map(automobile => {
                            return (
                            <option key={automobile.href} value={automobile.vin}>
                                {automobile.year} {automobile.color} {automobile.model.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeSalespersons}  required id="salesperson" name="salesperson" className="form-select">
                        <option value="">Choose an Employee</option>
                        {this.state.salespersons.map(salesperson => {
                            return (
                            <option key={salesperson.name} value={salesperson.name}>
                                {salesperson.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesForm;
