import React from "react";

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      phonenumber: "",
    };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAddress = this.handleChangeAddress.bind(this);
    this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log(data)
    
    

    const locationUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);
      this.setState({
        name: "",
        address: "",
        phonenumber: "",
      });
    }
  }

  handleChangeAddress(event) {
    const value = event.target.value;
    this.setState({ address: value });
  }
  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleChangePhoneNumber(event) {
    const value = event.target.value;
    this.setState({ phonenumber: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Customer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="address">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePhoneNumber} value={this.state.phonenumber} placeholder="PhoneNumber" required type="text" name="phonenumber" id="phonenumber" className="form-control" />
                <label htmlFor="address">Phone Number</label>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerForm