function SalesList(props) {
    console.log(props)
    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th className="display-5 fw-bold">Transactions</th>
            </tr>
          <tr>
            <th>Employee</th>
            <th>Employee ID</th>
            <th>Customer</th>
            <th>Vehicle</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody >
            {props.sales.map(sale => {
                return (
                    <tr key={sale.automobile}>
                        <td> {sale.name} </td>
                        <td> {sale.employeeid} </td>
                        <td> {sale.customer} </td>
                        <td> {sale.automobile} </td>
                        <td> {sale.price} </td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    );
  }
  
export default SalesList;