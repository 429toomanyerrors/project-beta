import React, {useState} from 'react'


// this was supposed to let you search by employee name to find their sales but it doesn't work 
//and I haven't linked it properly

function SalesHistoryList(props) {
    console.log(props)
    const sales = props.sales
    const [searchInput, setSearchInput] = useState("");
    const handleChange = (e) => {
      e.preventDefault();
      setSearchInput(e.target.value);
    };
    if (searchInput.length > 0) {
        sales.filter((sales) => {
        return sales.name.match(searchInput);
    });
    }


    return (
      <table className="table table-striped">
        <thead>
            <tr>
              <input
   type="text"
   placeholder="Search here"
   onChange={handleChange}
   value={searchInput} /></tr>
            <tr>
                <th className="display-5 fw-bold">Sales History</th>
            </tr>
            <tr>
            <th>Employee</th>
            <th>Employee ID</th>
            <th>Customer</th>
            <th>Vehicle</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody >
            {props.sales.map(sale => {
                return (
                    <tr key={sale.automobile}>
                        <td> {sale.name} </td>
                        <td> {sale.employeeid} </td>
                        <td> {sale.customer} </td>
                        <td> {sale.automobile} </td>
                        <td> {sale.price} </td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    );
  }

  export default SalesHistoryList;