function CustomerList(props) {
    console.log(props)
    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th className="display-5 fw-bold">Customer List</th>
            </tr>
          <tr>
            <th>Customers</th>
            <th>Adresses</th>
            <th>Phone Numbers</th>
          </tr>
        </thead>
        <tbody >
        {props.customers.map(customer => {
              return (
                  <tr key={customer.id}>
                      <td> {customer.name} </td>
                      <td> {customer.address} </td>
                      <td> {customer.phonenumber} </td>
                  </tr>
              );
          })}
        </tbody>
      </table>
    );
  }
  
export default CustomerList;