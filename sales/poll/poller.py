
import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO
#I remember making it the proper path breaks this so I just get to keep looking at this and thinking something broke
def poll():
    while True:
        print('Sales poller polling for data')
        try:
            response = requests.get("http://inventory-api:8000/api/automobiles/")
            #this port value is dumb and ugly and explained nowhere
            content = json.loads(response.content)
            for automobile in content['automobiles']:
                #you don't change color, I hate you
                AutomobileVO.objects.update_or_create(
                    import_href=automobile['href'],
                    defaults={
                        'vin': automobile['vin']
                    },
                    #If this becomes an automobile vs automobiles error I am going to scream
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
