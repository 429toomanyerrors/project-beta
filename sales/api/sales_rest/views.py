
from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesHistory



class AutomobileVOEncoder(ModelEncoder):
    model= AutomobileVO
    properties= [
        'vin'
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties= [
        'name',
        'address',
        'phonenumber',
        'id'
    ]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties=['name',
    'employeeid',
    'id']

class SalesHistoryListEncoder(ModelEncoder):
    model=SalesHistory
    properties= ['automobile',
    'salesperson',
    'customer',
    'price']
 #I have forgotten how this stuff wsorks, please don't break
    def get_extra_data(self, i):
        return {
            'automobile': i.automobile.vin,
            'salesperson':{
                'name': i.salesperson.name,
                'employeeid': i.salesperson.employeeid,
            },
            'customer': i.customer.name
        }
class SalesHistoryDetailEncoder(ModelEncoder):
    model=SalesHistory
    properties = ['automobile',
    'salesperson',
    'customer',
    'price',]
    encoders = {
        'automobile': AutomobileVOEncoder(),
        'salesperson': SalesPersonListEncoder(),
        'customer': CustomerListEncoder(),
    }

@require_http_methods(['GET', 'POST'])
def salesperson_list(request):
    if request.method == 'GET':
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            salespersons,
            encoder=SalesPersonListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson= SalesPerson.objects.create(**content)
            return JsonResponse(salesperson,
            encoder=SalesPersonListEncoder,
            safe= False)

        except:
            return JsonResponse({'message': 'Incorrect ID enetered. Releasing dogs.'})
#I sure hope id didn't actuall need to respond with proper codes or I'm boned

@require_http_methods(['DELETE'])
def salesperson_delete(request, pk):
    try:
        salesperson=SalesPerson.objects.get(id=pk)
        salesperson.delete()
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False
        )
    except SalesPerson.DoesNotExist:
        return JsonResponse({'message':'Not found, try under the floorboards'})


@require_http_methods(['GET', 'POST'])
def customer_list(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder=CustomerListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            customer= Customer.objects.create(**content)
            return JsonResponse(customer,
            encoder=CustomerListEncoder,
            safe= False)

        except:
            return JsonResponse({'message': 'The customer aint here, I plead the 5th'})
@require_http_methods(['DELETE'])
def customer_delete(request, pk):
    try:
        customer=Customer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )
    except Customer.DoesNotExist:
        return JsonResponse({'message':'You cannot kill that which was never alive.'})



@require_http_methods(['GET', 'POST'])
def sales_list(request):
    if request.method == 'GET':
        sales = SalesHistory.objects.all()
        return JsonResponse(
            {'sales':sales},
            encoder=SalesHistoryListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            vin= content['automobile']
            automobile= AutomobileVO.objects.get(vin=vin)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message':'Gone to the chop shop'})
        try:
            custname= content['customer']
            customer=Customer.objects.get(name=custname)
            content['customer']= customer
        except Customer.DoesNotExist:
            return JsonResponse({'message': 'Cant find that rat'})
        try:
            salesname= content['salesperson']
            salesperson= SalesPerson.objects.get(name=salesname)
            content['salesperson'] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse({'message':'Sleepin with the fishes, see.'})
        sales_history= SalesHistory.objects.create(**content)
        return JsonResponse(
            sales_history,
            encoder=SalesHistoryListEncoder,
            safe=False
        )
#get ready for an error with put, you know it's coming
@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_sales(request, pk):
    if request.method== 'GET':
        try:
            sales = SalesHistory.objects.get(id=pk)
            return JsonResponse(
                sales,
                encoder=SalesHistoryDetailEncoder,
                safe=False,
            )
        except SalesHistory.DoesNotExist:
            return JsonResponse({'message':'Books cooked, nothin here boss.'})
    elif request.method == 'DELETE':
        try:
            count,_=SalesHistory.objects.filter(id=pk).delete()
            return JsonResponse({'deleted': count > 0})
        except SalesHistory.DoesNotExist:
            return JsonResponse({'message':'Clever joke about Sales recod not being here'})
    else:
        content = json.loads(request.body)

        try:
            vin= content['automobile']
            automobile= AutomobileVO.objects.get(vin=vin)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message':'Different message about missing car'})
        try:
            custname= content['customer']
            customer=Customer.objects.get(name=custname)
            content['customer']= customer
        except Customer.DoesNotExist:
            return JsonResponse({'message': 'no customers here, you are likely to be eaten by a gru'})
        try:
            salesname= content['salesperson']
            salesperson= SalesPerson.objects.get(name=salesname)
            content['salesperson'] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse({'message':'Did not expect the spanish inquisition'})
            # Should this sales history bit be in a try except? No idea but my brain is mush so screw it
            #Everything else is why not this?- Famous last words of man debuggin until 3 am
        try:
            SalesHistory.objects.filter(id=pk).update(**content)
            sales = SalesHistory.objects.get(id=pk)
            return JsonResponse(
            sales,
            encoder=SalesHistoryDetailEncoder,
            safe=False
                )
        except SalesHistory.DoesNotExist:
            return JsonResponse({'message': 'What hath thou wrought?'})
