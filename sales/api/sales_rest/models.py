from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.TextField(unique=True)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employeeid= models.CharField(max_length=50, unique= True)

    def __str__(self):
        return self.name

class Customer(models.Model):
    name=models.CharField(max_length=50)
    address= models.CharField(max_length=300)
    phonenumber= models.CharField(max_length=20)

    def __str__(self):
        return self.name

class SalesHistory(models.Model):
    price = models.IntegerField()

    salesperson= models.ForeignKey(SalesPerson, 
    related_name='saleshistory', 
    on_delete=models.PROTECT)

    automobile= models.ForeignKey(
        AutomobileVO,
        related_name='saleshistory',
        on_delete=models.PROTECT)

    customer=models.ForeignKey(
        Customer,
        related_name='saleshistory',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return 'Transaction:' + self.salesperson + ' exchanged ' + self.automobile
    