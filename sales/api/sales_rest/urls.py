from django.urls import path

from .views import (
    customer_delete,
    customer_list,
    sales_list,
    salesperson_delete,
    salesperson_list,
    show_sales,
)

urlpatterns = [
    path('sales/', sales_list, name='sales_list'),
    path('customers/', customer_list, name='customer_list'),
    path('salespersons/', salesperson_list, name='salesperson_list'),
    path('salespersons/<int:pk>/', salesperson_delete, name='salesperson_delete'),
    path('customers/<int:pk>/', customer_delete, name= 'customer_delete'),
    path('sales/history/', show_sales, name= 'show_sales')
]

#The fact there is no sales delete is worrying, it's nested in show_sales.
#That doesn't make em feel better