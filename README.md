# CarCar

Team:

* Meleya Berhanu - Service
* Michael Sibley - Automobile Sales

## Design

## Service microservice
The service microsevice has three models. The first one is Technician model that take name and employee number of a technician. The second models is Service Appointment model that is used by service appointment form
Explain your models and integration with the inventory
microservice, here.


## Sales microservice

My microservice consists of four models. AutmobileVO, SalesPerson, Customer, and SalesHistory. AutomobileVO talks to the inventory service since that data is not within my service. SalesPerson sets up the employee who will be handling the transaction. Customer creates the customer which will be involved in the exchange. Finally, sales history is supposed to tie all those models together with the price of the transaction via multiple forigne keys which would not cull the database upon deletion.
 
We would be combining two bounded contexts in this interaction of Sales and Support. Our aggregate roots would be the Automobile model, SalesHistoryList, and whatever my partner names her client-facing service as the aggregate root is the coming together point for all our tiny pieces. Our value objects would be the sub attributes of our models. The entities would be specific models that each have seperate entitiy values and are not interchangebale.