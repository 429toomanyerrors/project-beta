import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


# Import models from service_rest, here.
# from service_rest.models import Something

from service_rest.models import AutomobileVIN

def get_AutomobileVIN():
    response = requests.get("http://inventory-api:8000/api/automobiles")
    content = json.loads(response.content)
    for automobile in content["automobiles"]:
        auto = { 
            "import_href" : automobile["href"],
            "vin": automobile["vin"]
        }
        print("Auto here",auto)
        AutomobileVIN.objects.update_or_create(
            **auto, 
            defaults={
                "vin": automobile["vin"]
            },
        )

def poll():
    while True:
        print(' NEW -- Service poller polling for data')
    
        try:
            get_AutomobileVIN()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()

