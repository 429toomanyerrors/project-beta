from django.contrib import admin
from .models import Technician, AutomobileVIN, ServiceAppointment

# Register your models here.
admin.site.register(Technician)
admin.site.register(AutomobileVIN)
admin.site.register(ServiceAppointment)