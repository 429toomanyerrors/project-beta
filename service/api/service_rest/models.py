from django.db import models
from django.urls import reverse

class AutomobileVIN(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=50)

    def get__str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class ServiceAppointment(models.Model):
    owners_name = models.CharField(max_length=100, unique=True)
    # vin = models.CharField(max_length=17,null=True)
    date = models.DateField(null= True, blank=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length=200)

    vinVO = models.ForeignKey(
        AutomobileVIN,
        related_name="service_appointments",
        on_delete=models.CASCADE,
    )

    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE,
    )


    def get_api_url(self):
        return reverse(
            "api_service_appointment", 
            kwargs={"pk": self.id}
            )