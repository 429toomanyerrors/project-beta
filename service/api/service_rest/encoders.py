from common.json import ModelEncoder

from .models import(
    Technician,
    ServiceAppointment,
)
    

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "owners_name",
        "vin",
        "date",
        "time",
        "reason",
        "technician",
    ]
    encoders ={
        "technician": TechnicianEncoder()
    }