from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import (
    AutomobileVIN,
    Technician,
    ServiceAppointment,
)

from .encoders import (
    TechnicianEncoder,
    ServiceAppointmentEncoder,
)

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: 
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)

            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

require_http_methods(["GET", "POST"])
def api_service_appointments(request):
    if request.method == "GET":
        service_appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"service_appointments": service_appointments},
            encoder=ServiceAppointmentEncoder
            safe=False
        )
    else: 
        try:

            content = json.loads(request.body)
            technician_id = content["technician_id"]
            technician = Technician.objects.get(pk=technician_id)
            content["technician"] = technician
            vin = content["vin"]
            vinVO = AutomobileVIN.objects.get(vin=vin)
            content["vinVO"] = vinVO
            del content["technician_id"]
            del content["vin"]
            print("content is ",content)
            service_appointment = ServiceAppointment.objects.create(**content)
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except: 
            response = JsonResponse(
                {"message": "Could not create a service appointment"}
            )
            response.status_code = 400
            return response 

@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_appointment(request, pk):
    if request.method == "GET":
        try:
            service_appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service_appointment = ServiceAppointment.objects.get(id=pk)
            service_appointment.delete()
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            service_appointment = ServiceAppointment.objects.get(id=pk)
            props = ["owners_name", "vin", "date", "time", "reason"]
            for prop in props:
                if prop in content:
                    setattr(service_appointment, prop, content[prop])
                service_appointment.save()
                return JsonResponse(
                    service_appointment,
                    encoder=ServiceAppointmentEncoder,
                    safe=False,
                )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
